import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import '@vcard-components/cells-theme-vcard'
import styles from './cells-ui-header-vcard-styles.js';
import '@bbva-web-components/bbva-badge-notification';
import '@cells-components/coronita-icons';
import '@cells-components/cells-icon';
import '@vcard-components/cells-util-behavior-vcard';
import '@vcard-components/cells-ui-slide-menu-vcard'
/**
This component ...

Example:

```html
<cells-ui-header-vcard></cells-ui-header-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiHeaderVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-ui-header-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      cls: {
        type: String
      },
      logo: {
        type: String
      },
      user: {
        type: Object
      },
      notifications: {
        type: Array
      },
      hideIconMenu: {
        type: Boolean
      },
      options: {
        type: Array
      },
      visible: {
        type: Boolean
      }

    };
  }
  // Initialize properties
  constructor() {
    super();
    this.cls = 'dark';
    this.user = this.extract(this, 'userSession', {});
    this.notifications = [];
    this.options = this.extract(this, 'userSession.opciones', []);
    this.updateComplete.then(() => {
      window.addEventListener('click', (e) => {
        this.hideMenuNotificactions();
        let slideMenu = this.shadowRoot.querySelector('cells-ui-slide-menu-vcard');
        slideMenu.hideMenuSlide();
      });
      this.shadowRoot.querySelector('.container-notification-box').addEventListener('click', (e) => {
        e.stopPropagation();
      });
    });
  }

  setUser(user){
    this.user = user;
    this.options = user.opciones;
    this.requestUpdate();
  }

  setNotifications(notifications) {
    this.notifications = notifications;
    this.requestUpdate();
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-header-vcard-shared-styles').cssText}
      ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
  }

  hideMenuNotificactions() {
    let containerNotificationBox = this.shadowRoot.querySelector('.container-notification-box');
    containerNotificationBox.classList.add('oculto');
  }

  showListNotifications() {
    let containerNotificationBox = this.shadowRoot.querySelector('.container-notification-box');
    if (this.notifications.length === 0) {
      containerNotificationBox.classList.add('oculto');
      return;
    }
    if (containerNotificationBox.classList.contains('oculto')) {
      containerNotificationBox.classList.remove('oculto');
    } else {
      containerNotificationBox.classList.add('oculto');
    }
  }

  showNotifications(e) {
    e.stopPropagation();
    let containerNotification = this.shadowRoot.querySelector('.container-notification');
    let containerNotificationBox = this.shadowRoot.querySelector('.container-notification-box');
    containerNotificationBox.style.left = `${(containerNotification.offsetLeft - 135)}px`;
    containerNotificationBox.style.top = `${(containerNotification.offsetTop + 43)}px`;
    this.showListNotifications();
  }

  iconMenuSelected(event) {
    event.stopPropagation();
    console.log('iconMenuSelected', event);
    this.dispatch(this.events.iconMenuSelected, {});
    let slideMenu = this.shadowRoot.querySelector('cells-ui-slide-menu-vcard');
    slideMenu.toggle();
  }

  logoSelected(event) {
    console.log('logoSelected', event);
    this.dispatch(this.events.logoSelected, {});
  }

  itemNotificationSelected(event) {
    console.log('itemNotificationSelected', event);
    this.dispatch(this.events.itemNotificationSelected, {});
  }

  profile(event) {
    console.log('profile', event);
    this.dispatch(this.events.goUserProfile, {});
  }

  nombreUsuario() {
    let nombreCompleto = this.extract(this, 'user.nombreCompleto', '');
    let codigoPerfil = this.extract(this, 'user.perfil.codigo', null);
    if(!this.isEmpty(nombreCompleto)) {
      let sections = nombreCompleto.split(' ');
      if(sections.length > 1){
        nombreCompleto = `${sections[0]} ${sections[1]}`;
      } 
    }
    if (codigoPerfil === this.ctts.perfiles.admin || codigoPerfil === this.ctts.perfiles.proveedor) {
      return html`${nombreCompleto.toUpperCase()}`;
    } else {
      let registro = this.extract(this, 'user.registro', '');
      return html`${nombreCompleto.toUpperCase()} <span class = "registro">${registro}</span>`;
    }
   
  }

  buildOficinaPerfil() {
    let codigoPerfil = this.extract(this, 'user.perfil.codigo', null);
    if(codigoPerfil === this.ctts.perfiles.admin || codigoPerfil === this.ctts.perfiles.proveedor) {
      return html`<a href = "javascript:;" class = "oficina" style = "text-transform:uppercase;" >${this.extract(this.user, 'perfil.nombre', '')}</a>`
    }
    return html`<a href = "javascript:;" class = "oficina" >${this.extract(this.user, 'oficina.nombre', '')} ${this.extract(this.user, 'oficina.codigo', '')}</a>`;
  }

  async closeNotification(notification,index) {
    console.log(notification);
    this.notifications = this.notifications.filter((item)=>{
        return item._id !== notification._id;
    });
    await this.requestUpdate();
    if(this.notifications.length === 0) {
      this.hideMenuNotificactions();
    }
    this.dispatch(this.events.removeNotification, notification);
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <header ?hidden = ${!this.visible} class = "${this.cls}" >
        <div class="section group">
          
          <div class="col span_6_of_12">
            <cells-icon ?hidden="${this.hideIconMenu}" @click = "${this.iconMenuSelected}" class = "menu-icon menu-icon-${this.cls}" size = "31" icon="coronita:menu" ></cells-icon>
            <div class = "logo" ><img @click = "${this.logoSelected}" src = "${this.logo}" ></div>
          </div>

          <div class="col span_6_of_12 user-header">
            <div class = "username" >
              ${this.nombreUsuario()}   
              <br>
              ${this.buildOficinaPerfil()}
            </div>
            <div @click = "${this.profile}" class = "userBubble">${this.user.userName ? this.user.nombreCompleto.charAt(0).toUpperCase() : html`&nbsp;`}</div>

            <div class="wrapper-notification">
              <div class="container-notification" @click = "${this.showNotifications}" >
              <cells-icon class ="icon-notify" size = "32" icon="coronita:alarm" ></cells-icon>
                <bbva-badge-notification class="abs bottom-right count-notify" text="${this.notifications.length}">
                </bbva-badge-notification>
              </div>
            </div>

          </div>

        </div>

        <div class = "container-notification-box oculto" >
          <div class = "speech-bubble" >
          
          ${this.notifications.map((notification, index) => html`
            <div class = "item-notification" data-item = "${JSON.stringify(notification)}"  @click = "${this.itemNotificationSelected}" >
            <span class="closebtn" @click = "${()=>this.closeNotification(notification, index)}" >&times;</span>    
              ${notification.mensaje}
            </div>
          `)}

          </div>
        </div>
          
      </header>

      <cells-ui-slide-menu-vcard id = "slideMenu" .options = "${this.options}" ></cells-ui-slide-menu-vcard>
      
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiHeaderVcard.is, CellsUiHeaderVcard);
