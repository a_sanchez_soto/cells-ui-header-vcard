import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --cells-ui-header-vcard; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

/* Styles */
.closebtn {
  color: #BDBDBD;
    font-weight: normal;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    position: absolute;
    top: 6px;
    right: 7px;
    transition: 0.3s;}
header {
  width: 100%;
  padding: 15px 20px;
  position: fixed;
  z-index: 30;
  font-size: 0.85rem; }
  header .logo h1 {
    margin: 0;
    font-size: 1rem;
    line-height: 0; }
  header .logo img {
    max-width: 70px;
    padding-top: 5px;
    cursor: pointer; }
  header .user-header {
    text-align: right; }
    header .user-header .username {
      font-size: 0.76rem;
      float: right;
      padding-top: 5px; }
    header .user-header .userBubble {
      cursor: pointer;
      width: 32px;
      height: 32px;
      border-radius: 24px;
      text-align: center;
      margin: 2px 7px 0px 0px;
      float: right;
      font-size: 1.1rem;
      padding-top: 6px; }

.registro {
  color: #1565c0; }

a.oficina {
  font-size: 0.6rem;
  text-decoration: none; }

.dark {
  background-color: #072146;
  color: #fff;
  border-bottom: 1px solid #072146; }
  .dark a.oficina {
    color: #fff; }
  .dark a.oficina:hover {
    color: #e1e1e1; }
  .dark .userBubble {
    background-color: #1464A5; }

.light {
  background-color: #F4F4F4;
  color: #000;
  border-bottom: 1px solid #BDBDBD; }
  .light a.oficina {
    color: #000; }
  .light .userBubble {
    background-color: #d5dee3;
    color: #666; }
  .light .icon-notify {
    color: #043263; }
  .light .item-notification {
    border-bottom: 1px solid #BDBDBD; }

.white {
  background-color: #fff;
  color: #121212;
  border-bottom: 1px solid #BDBDBD; }
  .white a.oficina {
    color: #000; }
  .white .userBubble {
    background-color: #d5dee3;
    color: #666; }
  .white .icon-notify {
    color: #043263; }
  .white .item-notification {
    border-bottom: 1px solid #BDBDBD; }

.wrapper-notification {
  margin: 0;
  float: right; }

.container-notification {
  margin: 0;
  float: right;
  position: relative;
  margin: 0px 18px 0;
  cursor: pointer; }

.icon-notify {
  margin-top: 1px; }

.count-notify {
  font-size: 0.55rem;
  margin-left: 4px; }

.container-notification-box {
  position: absolute;
  width: 300px;
  z-index: 10; }

.speech-bubble {
  color: #333;
  background: #fff;
  position: relative;
  padding: 0px;
  border: 1px solid #ccc;
  border-color: rgba(0, 0, 0, 0.2);
  -webkit-box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
  transition: visibility 0.1s linear; }

.oculto {
  visibility: hidden; }

.item-notification {
  cursor: pointer;
    padding: 20px 20px 15px 20px;
    border-bottom: 1px solid #e1e1e1;
    position: relative;
    line-height: 22px;
 }

.item-notification:last-child {
  border-bottom: 0px solid #fff; }

.menu-icon {
  cursor: pointer;
  display: inline-block;
  margin-right: 5px;
  margin-top: -9px; }

.menu-icon-dark {
  color: #fff; }

.menu-icon-light, .menu-icon-white {
  color: #043263; }

.logo {
  display: inline; }
`;